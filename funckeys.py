#!/usr/bin/python
import csv
from xivo_confd_client import Client
from tabulate import tabulate

c = Client('172.17.39.1',
           https=True,
           port=9486,
	   verify_certificate=False,
           username='greg',
           password='greg')
users = c.users.list()['items']
funckey_modele={"blf": False, "label": "Interception", "destination": {"type":"service","service":"pickup","user_id":999  }}

for user in users:
 user_id = c.users.get(user['id'])
 print "%s" % user['id']
 try:
  user_funckey_deux = c.users.relations(user_id).get_funckey(2)
 except:
  print "Pas de touche de fonction en place 2, on insère donc:"
  funckey_modele['destination']['user_id']=user_id['id']
  print "%s" % funckey_modele
  c.users.relations(user_id).add_funckey(2,funckey_modele)
 else:
  print "%s" % user_funckey_deux
