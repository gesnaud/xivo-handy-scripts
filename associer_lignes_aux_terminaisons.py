#!/usr/bin/python
# -*- coding: utf-8 -*-

from xivo_dao.helpers.db_manager import AsteriskSession
import requests
import argparse
import sys
import csv
import json
import logging
from subprocess import call

#configuration du script :
DEBUG = False
LOGFILE = 'associations.log'
CHAMP_MAC = 'mac'
CHAMP_NUMERO = 'number'

#usine : ne pas modifier
DEVICE_CONFIG = {"parent_ids": ["base", "defaultconfigdevice"],
                 "configdevice": "defaultconfigdevice",
                 "id": None,
                 "deletable": True,
                 "raw_config": {"exten_fwd_unconditional": "*21",
                                "protocol": "SCCP",
                                "exten_pickup_group": None,
                                "exten_fwd_busy": "*23",
                                "sccp_call_managers": {"1": {"ip":None}},
                                "X_key": "",
                                "exten_fwd_disable_all": "*20",
                                "exten_pickup_call": "*8",
                                "exten_dnd": "*25",
                                "exten_park": None,
                                "exten_fwd_no_answer": "*22",
                                "exten_voicemail": "*98"}
                }
PROVD_URL = 'http://localhost:8666/provd/'
HEADERS = {'Content-Type': 'application/vnd.proformatique.provd+json'}

session = None
logger = None

def main():
    (data_file, xivo_ip) = parse_args(sys.argv[1:])
    init_logging()
    data = extract_data_from_csv(data_file)
    global session
    session = AsteriskSession()
    associate_lines_to_devices(data, xivo_ip)
    session.close()
    reload_chan_sccp()

def init_logging():
    global logger
    logger = logging.getLogger(__name__)
    if(DEBUG):
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logfilehandler = logging.FileHandler(LOGFILE)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logfilehandler.setFormatter(formatter)
    logger.addHandler(logfilehandler)

def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ip')
    parser.add_argument('-f', '--file')
    parsed_args = parser.parse_args(args)
    if (not parsed_args.ip or not parsed_args.file):
        usage()
        sys.exit(1)
    return parsed_args.file, parsed_args.ip

def extract_data_from_csv(data_file):
    logger.info('Extraction des données de %s' % data_file)
    csvfile = open(data_file, 'r')
    return csv.DictReader(csvfile, delimiter=',')

def associate_lines_to_devices(data, ip_xivo):
    set_ip_xivo(ip_xivo)
    for row in data:
        try:
            device = retrieve_device(row[CHAMP_MAC])
            if not device['config'].startswith('autoprov'):
                logger.warning('Le téléphone d\'adresse %s est déjà configuré' % row[CHAMP_MAC])
                continue
            create_sccpdevice_in_db(row)
            create_config_in_jsondb(device['id'])
            old_config = device['config']
            update_device_in_jsondb(device)
            insert_device_in_linefeatures(row[CHAMP_NUMERO], device['id'])
        except Exception:
            logger.exception('Erreur en associant le téléphone %s à la ligne %s' % (row[CHAMP_MAC], row[CHAMP_NUMERO]))

def set_ip_xivo(ip_xivo):
    global DEVICE_CONFIG
    DEVICE_CONFIG['raw_config']['sccp_call_managers']['1']['ip'] = ip_xivo

def create_sccpdevice_in_db(data):
    formatted_mac = data[CHAMP_MAC].replace(':', '').upper()
    name = 'SEP%s' % formatted_mac
    logger.debug('Insertion d\'une entrée sccpdevice : %s, %s' % (name, data[CHAMP_NUMERO]))
    try:
        session.begin()
        session.execute('INSERT INTO sccpdevice(name, device, line) VALUES (:name, :name, :line)',
                        {'name': name, 'line': data[CHAMP_NUMERO]})
        session.commit()
    except Exception:
        logger.error('Impossible d\'insérer le téléphone %s pour la ligne %s dans la BDD' % (data[CHAMP_MAC], data[CHAMP_NUMERO]))
        raise

def retrieve_device(mac):
    logger.debug('Récupération du device dans la JSONDB')
    criteria = {'mac': mac}
    find_request = requests.get(PROVD_URL + 'dev_mgr/devices', params={'q': json.dumps(criteria)})
    if find_request.status_code != requests.codes.ok:
        logger.error('Impossible de récupérer les infos pour le téléphone %s dans la JSONDB : reçu statut %d de provd' % (mac, find_request.status_code))
        raise Exception('Erreur http : %d' % find_request.status_code)
    result = find_request.json()
    if 'devices' not in result or len(result['devices']) == 0:
        logger.error('Aucun téléphone d\'adresse %s n\'a été trouvé' % mac)
        raise Exception('Aucun téléphone : %s' % mac)
    if len(result['devices']) > 1:
        logger.error('Plusieurs configurations trouvées pour l\'adresse %s, terminaison non traitée' % mac)
        raise Exception('Plusieurs configurations : %s' % mac)
    return result['devices'][0]

def create_config_in_jsondb(device_id):
    global DEVICE_CONFIG
    DEVICE_CONFIG['id'] = device_id
    creation_request = requests.post(PROVD_URL + 'cfg_mgr/configs', data=json.dumps({'config': DEVICE_CONFIG}), headers=HEADERS)
    if creation_request.status_code != requests.codes.created:
        logger.error('Impossible de créer la config %s dans la JSONDB' % device_id)
        raise Exception('Erreur http: %d' % creation_request.status_code)

def update_device_in_jsondb(device):
    device['config'] = device['id']
    device['configured'] = True
    update_request = requests.put(PROVD_URL + 'dev_mgr/devices/%s' % device['id'], data=json.dumps({'device': device}), headers=HEADERS)
    if update_request.status_code != requests.codes.no_content:
        logger.error('Impossible de mettre à jour le device %s dans la JSONDB' % device['id'])
        raise Exception('Erreur http: %d' % update_request.status_code)

def insert_device_in_linefeatures(line_number, device_id):
    try:
        session.begin()
        session.execute('UPDATE linefeatures SET device = :device WHERE number = :number', {'device': device_id, 'number': line_number})
        session.commit()
    except Exception:
        logger.error('Impossible de mettre à jour la ligne %s avec le téléphone %s' % (line_number, device_id))
        raise

def reload_chan_sccp():
    call(['/usr/sbin/asterisk', '-rx', 'module reload chan_sccp.so'])

def usage():
    print 'Usage : %s -i IP_VOIX_XIVO -f FICHIER_DE_DONNEES' % sys.argv[0]

if __name__ == '__main__':
    main()
 
