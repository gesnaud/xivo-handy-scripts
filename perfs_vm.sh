#!/bin/bash

FILE_RESULT=/tmp/server_info.txt
DISK=$(df -h | grep 'dev' | cut -d ' ' -f 1 | grep /dev)
DAY=$(date +%Y%m%d)
echo "Information Systeme du "$DAY > $FILE_RESULT
echo "Nom de la machine: "$HOSTNAME >> $FILE_RESULT
echo "------ Informations Hardware ------" >> $FILE_RESULT
NB_PROC=$(cat /proc/cpuinfo | grep processor |wc -l)
echo -e "Nombre de processeurs: \033[31m"$NB_PROC"\033[00m" >> $FILE_RESULT

FREQ=$(cat /proc/cpuinfo | grep "cpu MHz" | uniq | cut -d ':' -f 2)
echo -e "Fréquence Processeur : \033[32m"$FREQ"\033[00m" >> $FILE_RESULT
#Memoire
MemTotal=$(cat /proc/meminfo | grep MemTotal | cut -d ':' -f2 | sed 's/^[ \t]*//' | cut -d ' ' -f 1)
MemFree=$(cat /proc/meminfo | grep MemFree | cut -d ':' -f2 | sed 's/^[ \t]*//')

echo -e "Memoire totale : \033[31m"$MemTotal"\033[00m" >> $FILE_RESULT
echo -e "Mémoire dispo : \033[31m"$MemFree"\033[00m" >> $FILE_RESULT

ARCH=$(uname -m)
echo "Architecture: "$ARCH>> $FILE_RESULT
echo "Disque: ">> $FILE_RESULT
echo -e "Partition /boot \033[32m"$(df -h /boot | grep /)"\033[00m" >> $FILE_RESULT
echo -e "Partition /systeme \033[32m"$(df -h $DISK | grep /)"\033[00m" >> $FILE_RESULT
echo -e "Partition /var \033[32m"$(df -h /var | grep /)"\033[00m" >> $FILE_RESULT

echo "----- Informations Systeme ------" >> $FILE_RESULT
OS_Server=$(uname -ar)
echo $OS_Server >> $FILE_RESULT

echo "-----Verification Perfomances VM -----">> $FILE_RESULT

echo "-----Performances en écriture (>à 200MB/S) -----" >> $FILE_RESULT
WRITE=$(for i in $(seq 1 1 3);
         do date 2>>$FILE_RESULT; dd bs=1M  count=256 if=/dev/zero of=test conv=fdatasync 2>>$FILE_RESULT ;
        sleep 60;
        done)
echo "-----Performances en lecture (> à 200MB/s) -----" >> $FILE_RESULT
READ=$(for i in $(seq 1 1 3);
         do date 2>>$FILE_RESULT; hdparm -t $DISK 2>>$FILE_RESULT ;
        sleep 30;
        done)

echo "-----Performances CPU(> à 400MB/s) -----" >> $FILE_RESULT
CPU=$(for i in $(seq 1 1 3);
         do date 2>>$FILE_RESULT; dd if=/dev/zero bs=1M count=1024 2>>$FILE_RESULT | md5sum 2>>$FILE_RESULT ;
        sleep 30;
        done)

