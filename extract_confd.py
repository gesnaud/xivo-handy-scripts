#!/usr/bin/python
import csv
from xivo_confd_client import Client
from tabulate import tabulate

c = Client('janine',
           https=True,
           port=44772,
           verify_certificate=False,
           username='slit',
           password='slit')
users = c.users.list()['items']
lines = c.lines.list()['items']
extensions = c.extensions.list()['items']
voicemails = c.voicemails.list()['items']
resultat = ""

with open('/var/tmp/export_user_extension.csv', 'wb') as csvfile:
 csv_user_exten= csv.writer(csvfile, delimiter=';')
 csv_user_exten.writerow(['Prenom','Nom','type','Numero interne'])

 for user in users:
  user_id = c.users.get(user['id'])
  user_lines = c.users.relations(user).list_lines()['items']

  try:
   user_voicemail = c.users.relations(user).get_voicemail()
  except:
   #print "Exception: user_email de %s sera vide" % (user['id'])
   email = 'vide'
  else:
   #print "Hello: %s" % (user_voicemail['voicemail_id'])
   user_voicemail_id = user_voicemail['voicemail_id']
   email = c.voicemails.get(user_voicemail_id)['email']
   #print "email: %s" % (email)
   #user_email = user_voicemail['email']

  #print "user email: %s" % (user_email)

  for user_line in user_lines:
   line = c.lines.get(user_line['line_id'])
   line_extension = c.lines.relations(line).list_extensions()['items']

   for extension in line_extension:
    extension = c.extensions.get(extension['extension_id'])
    if "from-extern" not in extension['context']:
     #print "%s;%s;%s;%s" % (user['firstname'],user['lastname'], line['protocol'], extension['exten'])
     #print "%s;%s;%s;%s;%s" % (user['firstname'].encode('utf-8').strip(),user['lastname'].encode('utf-8').strip(), line['protocol'], extension['exten'],user_email)
     print "%s;%s;%s;%s;%s" % (user['firstname'],user['lastname'], line['protocol'], extension['exten'],email)
     #csv_user_exten.writerow([user['firstname'], user['lastname'], line['protocol'], extension['exten']])
     #csv_user_exten.writerow([user['firstname'].encode('utf-8').strip(), user['lastname'].encode('utf-8').strip(), line['protocol'], extension['exten']])

