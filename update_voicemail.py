#!/usr/bin/python
import csv
from xivo_confd_client import Client

c = Client('127.0.0.1',
           https=True,
           port=9486,
	   verify_certificate=False,
           username='greg',
           password='greg')
voicemails = c.voicemails.list()['items']

for voicemail in voicemails:
 voicemail_id = c.voicemails.get(voicemail['id'])
 update_info = {'id': voicemail_id['id'], 'ask_password': False, 'timezone': 'eu-fr', 'language': 'fr_FR'}
 c.voicemails.update(update_info)
