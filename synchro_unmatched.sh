#!/bin/bash

rm -rf UNMATCHED_XIVO_USER_EPURE.txt

while read line
do
        echo $line > temp_line
        NEW_LINE=""
        #FIRSTNAME=$(grep -Eo "firstname=\w+ ," temp_line)
        FIRSTNAME=$(grep -Eo "firstname=.*,\Wlast" temp_line | awk -F "," '{print $1}')
        #LASTNAME=$(grep -Eo "lastname=\w+ ," temp_line)
        LASTNAME=$(grep -Eo "lastname=.*,\Wcall" temp_line | awk -F "," '{print $1}')
        #NUMBER=$(grep -Eo "number=\w+" temp_line)
        NUMBER=$(grep -Eo "line= { number=\w+" temp_line | tr ' ' '\n'|grep number)
        CONTEXT=$(grep -Eo "context=\w+" temp_line)
        #EMAIL=$(grep -Eo "email=\w+" temp_line)
        #EMAIL=$(grep -Eo ", email=*.*@*-*.fr" temp_line  | awk -F "=" '{print $2}')
        EMAIL=$(grep -Ewo "email=.*" temp_line|tr ', ' '\n'|grep email)

        if [ -n "$FIRSTNAME" ]
        then   
                NEW_LINE=$NEW_LINE$FIRSTNAME","
        else   
                NEW_LINE=$NEW_LINE"firstname=NULL,"
        fi

        if [ -n "$LASTNAME" ]
        then   
                NEW_LINE=$NEW_LINE$LASTNAME","
        else   
                NEW_LINE=$NEW_LINE"lastname=NULL,"
        fi

        if [ -n "$NUMBER" ]
        then   
                NEW_LINE=$NEW_LINE$NUMBER","
        else   
                NEW_LINE=$NEW_LINE"number=NULL,"
        fi

        if [ -n "$CONTEXT" ]
        then   
                NEW_LINE=$NEW_LINE$CONTEXT","
        else   
                NEW_LINE=$NEW_LINE"context=NULL"
        fi

        if [ -n "$EMAIL" ]
        then   
                NEW_LINE=$NEW_LINE$EMAIL
        else   
                NEW_LINE=$NEW_LINE"email=NULL"
        fi


        echo -ne $NEW_LINE"\n" >> UNMATCHED_XIVO_USER_EPURE.txt

done < $1
#rm -rf temp_line
