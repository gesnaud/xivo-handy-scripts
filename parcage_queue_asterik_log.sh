#!/bin/bash


for callid in `zgrep -E "\[... .. [012].:..:..\]" $1 | grep "\[$2@default:2\]"|awk -F "[- ]" '{print $5}'`
do

calling=`zgrep $callid $1 | grep "\[900@default:2\]"|awk -F "[/-]" '{print $3}'`
timestamp=`zgrep $callid $1 | grep "\[900@default:2\]"|awk -F "[ ]" '{print $1" "$2" "$3}'`

        echo -e "\n\nID d'appel: "$callid" & HOTEL: "$calling" "@ horodatage: "$timestamp\n"
        zgrep "$callid" $1 > $callid.txt

        while read line
        do

                case "$line" in
                        *res_musiconhold.c*Started* ) echo -ne "\t[DEBUG] Musique d'attente à: " ; echo $line | awk -F "[ ]" '{print $1" "$2" "$3}';;
                        *XIVO_QUEUELOG_EVENT=JOINEMPTY* ) echo -ne "\t[WARNING] FILE VIDE !!!";;
                        *Nobody* ) echo -ne "\t[DEBUG] Personne n'a décroché au bout de "; echo -n $line|awk -F " " '{print $10" "$11}';;
                        *app_dial.c*ringing* ) echo -ne "\t[DEBUG] Sur le téléphone :"; echo -n $line|awk -F " " '{print $6}';;
                        *app_queue.c*ringing* ) echo -ne "\t[DEBUG] On fait sonner l'agent :"; echo -n $line|awk -F " " '{print $6}';;
                        *agentcallback*answered* ) echo -ne "\t[NOTICE] ===========> On a décroché ($line)\n";;
                        *Spawn*queue* ) echo -ne "\t[DEBUG] L'appel est raccroché. Horaire:"; echo $line | awk -F "[ ]" '{print $1" "$2" "$3}';;
                        *pbx.c*queue-timeout* ) echo -ne "\t[WARNING] L'appel est resté trop longtemps dans la file. TIME-OUT.";;
                esac

        done < $callid.txt
        rm -rf $callid.txt
done

