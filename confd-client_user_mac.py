#!/usr/bin/python
from xivo_confd_client import Client

c = Client('127.0.0.1',
            https=False,
            port=9487,
            verify_certificate=False,
            username='slit',
            password='slit')
users = c.users.list()['items']
lines = c.lines.list()['items']
devices = c.devices.list()['items']


for user in users:
 user_id = c.users.get(user['id'])
 user_lines = c.users.relations(user).list_lines()['items']

 for user_line in user_lines:
  line = c.lines.get(user_line['line_id'])
 try:
  user_device = c.lines.relations(line).get_device()
 except:
  print "Exception: %s pas de terminaisons" % (user['id'])
 else:

  for device in devices:
   if device['id'] == user_device['device_id']:
    print "%s,%s,%s" % (user['firstname'], user['lastname'], device['mac'])
